import '../styles/index.css'
import '../public/assets/css/screen.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
